module Lib
    ( solve
    ) where

import Numeric.LinearAlgebra
import Graphics.EasyPlot as GEP

type RealFunction = (Double -> Double)

n :: Int
k :: Double
l :: Double
r :: Double
h :: Double
n = 50 --elements count
k = -1.0 --k constant
l = 0.0 --lower boundary
r = 1.0 --upper boundary
h = (r-l)/(fromIntegral n) --interval size

-- Integration epsilon
epsilon :: Double
epsilon = 0.00001

-- Square integration 
integrate :: RealFunction -> Double
integrate g = sum [(g x)*epsilon | x<-[l,l+epsilon..r]]

-- Dirichlet shift functions
u_hat :: RealFunction
u_hat_prim :: RealFunction
u_hat x = 5*(1-x)
u_hat_prim x = -5

-- f function
f :: RealFunction
f x = 5*x - 10 

-- Base function generator
base :: Int -> RealFunction
baseHelper :: Double -> Double -> Double -> RealFunction
base i = baseHelper ((j-1)*h) (h*j) ((j+1)*h)
    where j = fromIntegral i
baseHelper low mid up x
    | x < mid && x >= low = (x-low)/h
    | x >= mid && x <= up = (up-x)/h
    | otherwise = 0.0

-- Generator of base function derivative
basePrim :: Int -> RealFunction
basePrimHelper :: Double -> Double -> Double -> RealFunction
basePrim i = basePrimHelper ((j-1)*h) (h*j) ((j+1)*h)
    where j = fromIntegral i
basePrimHelper low mid up x
    | x < mid && x >= low = 1.0/h
    | x > mid && x <= up = -1.0/h
    | otherwise = 0.0

-- B(u, v) calculation
calcB :: RealFunction -> RealFunction -> RealFunction -> RealFunction -> Double
calcB u v up vp = (integrate (\x -> k*(up x)*(vp x))) + (integrate (\x-> (up x)*(v x)))

-- L(V) calculation
calcL :: RealFunction -> RealFunction -> Double
calcL v vp = (integrate (\x-> (f x) * (v x))) + 3.0*k*(v r)

-- Constructs B matrix
mB :: Matrix Double
mB = fromLists [[calcB (base col) (base row) (basePrim col) (basePrim row) | col <- [1..n]] | row <- [1..n]]

-- Constructs L matrix
mL :: Vector Double
mL = (vector [calcL (base row) (basePrim row) | row <- [1..n]]) - (vector [calcB u_hat (base row) u_hat_prim (basePrim row) | row <- [1..n]])

-- Solving Bx=L with library funtion
solved ::  [Double]
solved = toList (mB <\> mL)

-- Construction of BVP solution
u :: RealFunction
u x = (u_hat x) + (sum [(base (i+1) x) * (solved !! i) | i <- [0..(n-1)]] )

-- IO action for plotting with GNU plot
draw :: IO ()
draw = do
  _ <- GEP.plot (GEP.PNG "data/wykres.png") $ GEP.Function2D [GEP.Title "u(x)"] [GEP.Range l r] u
  return ()

-- IO action, prints w and plots result
solve :: IO ()
solve = do
  putStrLn "-------RESULTS-------"
  mapM (putStrLn . show) solved
  putStrLn "------PLOTTING-------"
  putStrLn "Saved to data/wykres.png"
  draw